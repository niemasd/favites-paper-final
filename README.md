# favites-paper-final

## Update May 3, 2021
It was recently brought to our attention that the rows in Table S4, "HIV Simulation Parameters (epidemiological model)", are swapped: the top row corresponds to the San Diego simulation parameters, and the bottom row corresponds to the Uganda simulation parameters. Note that the parameters are correct in the main paper as well as in the FAVITES `CONFIG` files that are stored in this repository. Further, only two simulation parameters differ between the San Diego and Uganda simulations: (1) the number of seeds, and (2) the rate at which individuals stop ART.
