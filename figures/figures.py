#! /usr/bin/env python3
from copy import copy
from gzip import open as gopen
from matplotlib.collections import PolyCollection
from matplotlib.patches import Patch
from matplotlib.ticker import FuncFormatter
from pandas import DataFrame
from pickle import load
from seaborn import distplot,pointplot,scatterplot,set_context,set_style,violinplot
import matplotlib.pyplot as plt
EART = r'E_{ART}'
ED = r'E_{d}'

### set things up ###
RC = {"font.size":12,"axes.titlesize":16,"axes.labelsize":14,"legend.fontsize":10,"xtick.labelsize":10,"ytick.labelsize":10}
set_context("paper", rc=RC); set_style("ticks")
from matplotlib import rcParams; rcParams['font.family'] = 'serif'
real_vs_sim_pal = {'SD':'#000000', 'SIM':'#666666', 'SIM_FT':'#000000', 'SIM_SEED':'#000000', 'SIM_SAMPLE':'#000000', 'SIM_SUB':'#000000', 'UG':'#cccccc', 'SIM_UG_SUB':'#cccccc'}
real_vs_sim_handles = [Patch(color=real_vs_sim_pal['SD'],label="San Diego"), Patch(color=real_vs_sim_pal['UG'],label='Uganda')]
real_vs_sim_linestyles = {'SD':'--','SIM':'-','SIM_SUB':'-','UG':'--','SIM_UG_SUB':'-'}
true_vs_inferred_pal = {'TRUE':'#000000', 'FT':'#cccccc', 'SD':'#000000', 'UG':'#cccccc', 'AVG':'#666666'}
true_vs_inferred_handles = [Patch(color=true_vs_inferred_pal['TRUE'],label='True'), Patch(color=true_vs_inferred_pal['FT'],label='Inferred')]
art_vals_pal = {0.125:'#000000', 0.25:'#262626', 0.5:'#4d4d4d', 1:'#737373', 2:'#999999', 4:'#bfbfbf', 8:'#e6e6e6'}
art_vals_handles = [Patch(color=art_vals_pal[k], label=r'$%s=%s$' % (EART,str(1./k).replace('.0',''))) for k in sorted(art_vals_pal.keys())]
ninety_ninety_ninety_color = '#86c5da'
ref_color = '#000000'
degree_pal = {2:'#bfbfbf', 4:'#808080', 8:'#404040', 16:'#000000'}
degree_handles = [Patch(color=degree_pal[d],label=r'$%s=%d$' % (ED,d)) for d in degree_pal]
rf_short_linestyles = {'RF':'-', 'SHORT_BRANCHES':':'}
effectiveness_pal = {'expected':'#cccccc', 'treecluster':'#666666', 'tn93':'#000000'}
effectiveness_handles = [Patch(color=effectiveness_pal['expected'],label="Expected"), Patch(color=effectiveness_pal['treecluster'],label="TreeCluster"), Patch(color=effectiveness_pal['tn93'],label="HIV-TRACE")]
mutation_tr = {'REAL':'Real', 'TRUNCNORM':'Truncated Normal', 'LOGNORM':'Log-Normal', 'EXPON':'Exponential', 'CONST':'Constant', 'GAMMA':'Gamma'}
mutation_order = ['REAL', 'TRUNCNORM', 'EXPON', 'CONST', 'GAMMA', 'LOGNORM']

# make x tick numbers integers instead of floats
def x_tick_int(div=10.):
    plt.gca().xaxis.set_major_formatter(FuncFormatter(lambda x, _: int(x/div)))

# get average of list
def avg(x):
    return sum(x)/float(len(x))

# violin plot outline
def violinplot_outline(c):
    ax = plt.gca()
    for art in ax.get_children():
        if isinstance(art, PolyCollection):
            art.set_edgecolor(c)

### plot pairwise distance distributions vs. expected time to ART ###
'''
rc = copy(RC); rc["legend.fontsize"]=14; rc["xtick.labelsize"]=12; rc["ytick.labelsize"]=14; set_context("paper", rc=rc)
data = {}
data['JC69'] = load(gopen('DATA.JC69.pkl.gz'))
data['TN93'] = load(gopen('DATA.TN93.pkl.gz'))
data['TN93+G'] = load(gopen('DATA.TN93G.pkl.gz'))
for dist in data:
    for m in ['BA','ER','WS']:
        for d in [2,4,8,16]:
            fig = plt.figure()
            df = {'x':[], 'y':[]}
            for r in [0.125,0.25,0.5,1,2,4,8]:
                df['y'] += data[dist][m][d][r]; df['x'] += [1./r]*len(data[dist][m][d][r])
            df['y'] += data[dist]['UG']; df['x'] += ['UG']*len(data[dist]['UG'])
            for k in data[dist]['REAL']:
                df['y'] += data[dist]['REAL'][k]; df['x'] += ['Real %s'%k]*len(data[dist]['REAL'][k])
            df = DataFrame(df)
            order = ['Real SD']+sorted({k for k in df['x'] if isinstance(k,float)},reverse=True)+['Real UG','UG']
            violinplot(x='x', y='y', order=order, palette={k:'#000000' for k in order}, data=df, inner=None)
            for x,k in enumerate(order):
                scatterplot(x=[x], y=[avg(df.loc[df['x'] == k]['y'])], color=true_vs_inferred_pal['AVG'])
            if m == 'BA' and d == 4:
                if dist == 'Tree':
                    title = "Pairwise Tree Distance vs. ART"
                else:
                    title = "%s Distance vs. ART"%dist
                plt.title(title, size=22)
            else:
                plt.title(r"%s vs. ART (%s, $%s=%d$)" % (dist,m,ED,d), size=22)
            plt.xlabel(r"$%s$ (years)"%EART, size=18)
            plt.ylabel("Pairwise %s Distance"%dist, size=18)
            w,h = fig.get_size_inches(); fig.set_size_inches(w*1.5, h)
            plt.show()
            if m == 'BA' and d == 4:
                fig.savefig('%s_distance.pdf'%dist.lower(), format='pdf', bbox_inches='tight')
            else:
                fig.savefig('%s_%s_distance_degree_%d.pdf'%(m.lower(),dist.lower(),d), format='pdf', bbox_inches='tight')
            plt.close()
set_context("paper", rc=RC)
'''

### plot branch length and pairwise distance distributions vs. expected time to ART ###
'''
rc = copy(RC); rc["legend.fontsize"]=14; rc["xtick.labelsize"]=12; rc["ytick.labelsize"]=14; set_context("paper", rc=rc)
data = {}
data['BL'] = {}
data['BL']['TRUE'] = load(gopen('DATA.BRANCH_LENGTHS_TRUE.pkl.gz'))
data['BL']['FT'] = load(gopen('DATA.BRANCH_LENGTHS_FT.pkl.gz'))
data['PAIR'] = {}
data['PAIR']['TRUE'] = load(gopen('DATA.TREEDISTS_TRUE.pkl.gz'))
data['PAIR']['FT'] = load(gopen('DATA.TREEDISTS_FT.pkl.gz'))
delta = {'TRUE':-.2,'FT':.2}
tr = {'BL':'Branch Length', 'PAIR': 'Pairwise Tree Distance'}
tr_f = {'BL':'branch_length', 'PAIR':'pairwise_tree_distance'}
for dist in data:
    for m in ['BA','ER','WS']:
        for d in [2,4,8,16]:
            fig = plt.figure()
            df = {'x':[], 'type':[], 'y':[]}
            for r in [0.125,0.25,0.5,1,2,4,8]:
                for k in data[dist]:
                    df['y'] += data[dist][k][m][d][r]; df['x'] += [1./r]*len(data[dist][k][m][d][r]); df['type'] += [k]*len(data[dist][k][m][d][r])
            for k in data[dist]:
                df['y'] += data[dist][k]['UG']; df['x'] += ['UG']*len(data[dist][k]['UG']); df['type'] += [k]*len(data[dist][k]['UG'])
            order = ['Real SD']+sorted({k for k in df['x'] if isinstance(k,float)},reverse=True)+['Real UG','UG']
            df = DataFrame(df)
            violinplot(x='x', y='y', order=order, hue='type', split=True, palette=true_vs_inferred_pal, data=df, inner=None)
            df2 = {'x':[], 'type':[], 'y':[]}
            for k in data[dist]['FT']['REAL']:
                df2['y'] += data[dist]['FT']['REAL'][k]; df2['x'] += ['Real %s'%k]*len(data[dist]['FT']['REAL'][k]); df2['type'] += ['FT']*len(data[dist]['FT']['REAL'][k])
            df2 = DataFrame(df2)
            violinplot(x='x', y='y', order=order, hue='type', palette=true_vs_inferred_pal, data=df2, inner=None)
            violinplot_outline('#000000')
            for x,k in enumerate(order):
                if isinstance(k,str) and k.startswith('Real '):
                    scatterplot(x=[x], y=[avg(df2.loc[df2['x'] == k]['y'])], color=true_vs_inferred_pal['AVG'])
                else:
                    for s in ['TRUE','FT']:
                        scatterplot(x=[x+delta[s]], y=[avg(df.loc[(df['x'] == k) & (df['type'] == s)]['y'])], color=true_vs_inferred_pal['AVG'])
            if m == 'BA' and d == 4:
                plt.title(r"%s vs. ART" % tr[dist], size=22)
            else:
                plt.title(r"%s vs. ART (%s, $%s=%d$)" % (tr[dist],m,ED,d), size=22)
            plt.xlabel(r"$%s$"%EART, size=18)
            plt.ylabel(tr[dist], size=18)
            legend = plt.legend(handles=true_vs_inferred_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
            w,h = fig.get_size_inches(); fig.set_size_inches(w*1.5, h)
            plt.show()
            if m == 'BA' and d == 4:
                fig.savefig('%s.pdf'%tr_f[dist], format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
            else:
                fig.savefig('%s_%s_degree_%d.pdf'%(tr_f[dist],m.lower(),d), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
            plt.close()
set_context("paper", rc=RC)
'''

### plot number (AU+CU)/(AT+CT) vs. expected time to ART ###
'''
data = load(gopen('DATA.NUM_PER_TIME.pkl.gz'))
for m in ['BA']:#,'ER','WS']:
    fig = plt.figure()
    plt.scatter(x=[3.],y=[1.041],color='#000000',marker='P',s=60)
    for d in [2,4,8,16]:
        x = []; y = []
        for r in [0.125,0.25,0.5,1,2,4,8]:
            end = max(data[m][d][r].keys()); au = data[m][d][r][end]['AU']; cu = data[m][d][r][end]['CU']; at = data[m][d][r][end]['AT']; ct = data[m][d][r][end]['CT']
            y += [(au[i]+cu[i])/(at[i]+ct[i]) if at[i]+ct[i] > 0 else au[i]+cu[i] for i in range(len(au))]; x += [1./r]*len(au)
        ax = pointplot(x=x, y=y, color=degree_pal[d])
    ax.invert_xaxis()
    xlim = ax.get_xlim()
    if m == 'BA':
        plt.title("Untreated/Treated vs. ART")
    else:
        plt.title("Untreated/Treated vs. ART (%s)"%m)
    plt.xlabel(r"$%s$ (years)"%EART)
    plt.ylabel("Untreated/Treated")
    plt.plot([-1000,1000],[1,1],linestyle='--',color=ref_color)
    plt.plot([-1000,1000],[(1.-.9**3)/(.9**3),(1.-.9**3)/(.9**3)],linestyle='--',color=ninety_ninety_ninety_color)
    ax.set_yscale('log')
    plt.xlim(xlim)
    legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
    plt.show()
    if m == 'BA':
        fig.savefig('untreated_vs_treated.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    else:
        fig.savefig('%s_untreated_vs_treated.pdf'%m.lower(), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    plt.close()
'''

### plot number (AU+CU)/(AT+CT) vs. time (vary expected time to ART) ###
'''
data = load(gopen('DATA.NUM_PER_TIME.pkl.gz'))
for m in ['BA','ER','WS']:
    for d in [2,4,8,16]:
        fig = plt.figure()
        for r in [0.125,0.25,0.5,1,2,4,8]:
            x = []; y = []
            for t in data[m][d][r]:
                au = data[m][d][r][t]['AU']; cu = data[m][d][r][t]['CU']; at = data[m][d][r][t]['AT']; ct = data[m][d][r][t]['CT']
                y += [(au[i]+cu[i])/(at[i]+ct[i]) if at[i]+ct[i] > 0 else au[i]+cu[i] for i in range(len(au))]; x += [t]*len(au)
            ax = pointplot(x=x, y=y, color=art_vals_pal[r], markers='', ci=None, scale=0.5)
        x_tick_int()
        xlim = ax.get_xlim()
        plt.plot([-1000,1000],[1,1],linestyle='--',color=ref_color)
        plt.plot([-1000,1000],[(1.-.9**3)/(.9**3),(1.-.9**3)/(.9**3)],linestyle='--',color=ninety_ninety_ninety_color)
        ax.set_yscale('log')
        plt.xlim(xlim)
        plt.xlim(xmin=20)
        plt.ylim(ymax=10)
        if m == 'BA' and d == 4:
            plt.title("Untreated/Treated vs. Time")
        else:
            plt.title(r"Untreated/Treated vs. Time (%s, $%s=%d$)" % (m,ED,d))
        plt.xlabel("Time (years)")
        plt.ylabel("Untreated/Treated")
        tick_labels = ax.xaxis.get_ticklabels()
        for i in range(len(tick_labels)):
            if i % 10 != 0:
                tick_labels[i].set_visible(False)
        if m == 'BA' and d == 4:
            ax.text(90, 3.5,r'$%s=8$'%EART,fontsize=9)
            ax.text(90, 1.7,r'$%s=4$'%EART,fontsize=9)
            ax.text(90, 0.83,r'$%s=2$'%EART,fontsize=9)
            ax.text(90, 0.41,r'$%s=1$'%EART,fontsize=9)
            ax.text(88, 0.2,r'$%s=0.5$'%EART,fontsize=9)
            ax.text(86.75, 0.10,r'$%s=0.25$'%EART,fontsize=9)
            ax.text(85.4, 0.05,r'$%s=0.125$'%EART,fontsize=9)
        legend = plt.legend(handles=art_vals_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
        plt.show()
        if m == 'BA' and d == 4:
            fig.savefig('aucu_div_atct_vs_time_art.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
        else:
            fig.savefig('aucu_div_atct_vs_time_art_%s_ed%d.pdf' % (m.lower(),d), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
        plt.close()
'''

### plot num_infected vs. ART ###
'''
data = load(gopen('DATA.NUM_INFECTED.pkl.gz'))
for m in ['BA','ER','WS']:
    fig = plt.figure()
    for d in [2,4,8,16]:
        x = []; y = []
        for r in [0.125,0.25,0.5,1,2,4,8]:
            y += data[m][d][r]; x += [1./r]*len(data[m][d][r])
        ax = pointplot(x=x, y=y, color=degree_pal[d])
    ax.invert_xaxis()
    xlim = ax.get_xlim()
    if m == 'BA':
        plt.title("Number Infected vs. ART")
    else:
        plt.title("Number Infected vs. ART (%s)"%m)
    plt.xlabel(r"$%s$ (years)"%EART)
    plt.ylabel("Number of Infected Individuals")
    plt.ylim(0,100000)
    plt.plot([-1000.,1000.],[15000,15000],linestyle='--',color=ref_color)
    plt.xlim(xlim)
    legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
    plt.show()
    if m == 'BA':
        fig.savefig('num_infected.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    else:
        fig.savefig('%s_num_infected.pdf'%m.lower(), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    plt.close()
'''

### plot average branch length vs. ART ###
'''
data = load(gopen('DATA.AVG_BL.pkl.gz'))
rc = copy(RC); rc["legend.fontsize"]=12; rc["xtick.labelsize"]=14; rc["ytick.labelsize"]=14; set_context("paper", rc=rc)
for m in ['BA','ER','WS']:
    fig = plt.figure()
    for d in [2,4,8,16]:
        x = []; y = []
        for r in [0.125,0.25,0.5,1,2,4,8]:
            y += data[m][d][r]; x += [1./r]*len(data[m][d][r])
        ax = pointplot(x=x, y=y, color=degree_pal[d])
    ax.invert_xaxis()
    if m == 'BA':
        plt.title("Average Branch Length vs. ART", size=22)
    else:
        plt.title("Average Branch Length vs. ART (%s)"%m, size=22)
    plt.xlabel(r"$%s$ (years)"%EART, size=18)
    plt.ylabel("Average Branch Length", size=18)
    legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
    plt.show()
    if m == 'BA':
        fig.savefig('avg_bl.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    else:
        fig.savefig('%s_avg_bl.pdf'%m.lower(), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    plt.close()
set_context("paper", rc=RC)
'''

### plot RF distance with proportion of branches with <= expected mutation vs. ART ###
'''
data = {}
data['RF'] = load(gopen('DATA.RF_BA.pkl.gz')) #data['RF'] = load(gopen('DATA.RF.pkl.gz'))
data['SHORT_BRANCHES'] = load(gopen('DATA.SHORT_BRANCHES.pkl.gz'))
rc = copy(RC); rc["legend.fontsize"]=12; rc["xtick.labelsize"]=14; rc["ytick.labelsize"]=14; set_context("paper", rc=rc)
for m in ['BA']: #for m in ['BA','ER','WS']:
    fig = plt.figure()
    for k in data:
        for d in [2,4,8,16]:
            x = []; y = []
            for r in [0.125,0.25,0.5,1,2,4,8]:
                y += data[k][m][d][r]; x += [1./r]*len(data[k][m][d][r])
            ax = pointplot(x=x, y=y, color=degree_pal[d], linestyles=rf_short_linestyles[k])
    ax.invert_xaxis()
    if m == 'BA':
        plt.title("RF, Short Branches vs. ART", size=22)
    else:
        plt.title("RF, Short Branches vs. ART (%s)"%m, size=22)
    plt.xlabel(r"$%s$ (years)"%EART, size=18)
    legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
    plt.show()
    if m == 'BA':
        fig.savefig('rf_and_prop_branch_lte_1_exp_mut.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    else:
        fig.savefig('%s_rf_and_prop_branch_lte_1_exp_mut.pdf'%m.lower(), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    plt.close()
set_context("paper", rc=RC)
'''

### plot effectiveness vs. ART ###
'''
data = load(gopen('DATA.EFFECTIVENESS_BA.pkl.gz')) #data = load(gopen('DATA.EFFECTIVENESS.pkl.gz'))
rc = copy(RC); rc["legend.fontsize"]=12; rc["xtick.labelsize"]=14; rc["ytick.labelsize"]=14; set_context("paper", rc=rc)
for m in ['BA']: #for m in ['BA','ER','WS']:
    for d in [2,4,8,16]:
        fig = plt.figure()
        for k in ['expected','treecluster','tn93']:
            x = []; y = []
            for r in [0.125,0.25,0.5,1,2,4,8]:
                y += data[m][d][r][k]; x += [1./r]*len(data[m][d][r][k])
            ax = pointplot(x=x, y=y, color=effectiveness_pal[k])
        ax.invert_xaxis()
        if m == 'BA':
            plt.title(r"Effectiveness $\left(%s=%d\right)$" % (ED,d), size=22)
            plt.ylim(0,0.15)
        else:
            plt.title(r"Effectiveness (%s, %s=%d$)" % (m,ED,d), size=22)
        plt.xlabel(r"$%s$ (years)"%EART, size=18)
        if d == 2:
            plt.ylabel("Average Number of Infections", size=18)
        else:
            ax.set_yticks([])
        legend = plt.legend(handles=effectiveness_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
        plt.show()
        if m == 'BA':
            fig.savefig('effectiveness_degree_%s.pdf'%d, format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
        else:
            fig.savefig('%s_effectiveness_degree_%s.pdf'%(m.lower(),d), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
        plt.close()
set_context("paper", rc=RC)
'''

### plot mutation rate model violin plots ###
'''
rc = copy(RC); rc["legend.fontsize"]=14; rc["xtick.labelsize"]=12; rc["ytick.labelsize"]=14; set_context("paper", rc=rc)
data = load(gopen('DATA.MUTATIONMODEL.pkl.gz'))
delta = {'TRUE':-.2,'FT':.2}
order = [mutation_tr[m] for m in mutation_order]
tr = {'JC69':'Pairwise JC69 Distance', 'BL':'Branch Length', 'TREEDISTS':'Pairwise Tree Distance'}
for dist in ['JC69','BL','TREEDISTS']:
    if dist == 'JC69':
        pal = {m:'#000000' for m in order}; hue = None; df = {'x':[], 'y':[]}; split=False
    else:
        pal = true_vs_inferred_pal; hue = 'type'; df = {'x':[], 'y':[], 'type':[]}; split=True
    fig = plt.figure()
    for m in mutation_order:
        if dist == 'JC69':
            df['y'] += data['JC69'][m]; df['x'] += [mutation_tr[m]]*len(data['JC69'][m])
        elif m != 'REAL':
            df['y'] += data['%s_TRUE'%dist][m]; df['type'] += ['TRUE']*len(data['%s_TRUE'%dist][m]); df['x'] += [mutation_tr[m]]*len(data['%s_TRUE'%dist][m])
            df['y'] += data['%s_FT'%dist][m]; df['type'] += ['FT']*len(data['%s_FT'%dist][m]); df['x'] += [mutation_tr[m]]*len(data['%s_FT'%dist][m])
    df = DataFrame(df)
    violinplot(x='x', y='y', order=order, palette=pal, hue=hue, data=df, inner=None, split=split)
    if dist != 'JC69':
        df2 = DataFrame({'y':data['%s_FT'%dist]['REAL'], 'x':[mutation_tr['REAL']]*len(data['%s_FT'%dist]['REAL']), 'type':['FT']*len(data['%s_FT'%dist]['REAL'])})
        violinplot(x='x', y='y', order=order, palette=pal, hue=hue, inner=None, data=df2)
    violinplot_outline('#000000')
    for x,m in enumerate(mutation_order):
        if dist == 'JC69':
            scatterplot(x=[x], y=[avg(df.loc[df['x'] == mutation_tr[m]]['y'])], color=true_vs_inferred_pal['AVG'])
        elif m == 'REAL':
            scatterplot(x=[x], y=[avg(df2.loc[df2['x'] == mutation_tr[m]]['y'])], color=true_vs_inferred_pal['AVG'])
        else:
            for s in ['TRUE','FT']:
                scatterplot(x=[x+delta[s]], y=[avg(df.loc[(df['x'] == mutation_tr[m]) & (df['type'] == s)]['y'])], color=true_vs_inferred_pal['AVG'])
    plt.xlabel("Mutation Rate Model", size=18)
    plt.ylabel(tr[dist], size=18)
    plt.title("%s vs. Mutation Rate Model"%tr[dist], size=22)
    w,h = fig.get_size_inches(); fig.set_size_inches(w*2, h)
    if dist != 'JC69':
        legend = plt.legend(handles=true_vs_inferred_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
    plt.show()
    if dist == 'JC69':
        fig.savefig('mutationrate_jc69.pdf', format='pdf', bbox_inches='tight')
    else:
        fig.savefig('mutationrate_%s.pdf'%dist.lower(), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
set_context("paper", rc=RC)
'''
